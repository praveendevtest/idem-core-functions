import pytest


@pytest.mark.asyncio
async def test_str_to_json(hub):
    data = '{ "cluster_name": "idem-eks-test", "region": "ap-south-1" }'
    result = await hub.exec.core.conversion.to_json(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert (
        '"{ \\"cluster_name\\": \\"idem-eks-test\\", \\"region\\": \\"ap-south-1\\" }"'
        == ret["data"]
    )


async def test_dict_to_json(hub):
    # Creating a dictionary
    dict = {
        ("x", "y", "z"): "Elder Wand",
        2: "Expecto",
        3: "Patronum",
        4: "Avada",
        5: "Kedavra",
        6: float("nan"),
    }
    result = await hub.exec.core.conversion.to_json(
        data=dict, skip_keys=True, allow_nan=True, indent=4, separators=(". ", " = ")
    )
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    output = (
        "{\n"
        '    "2" = "Expecto". \n'
        '    "3" = "Patronum". \n'
        '    "4" = "Avada". \n'
        '    "5" = "Kedavra". \n'
        '    "6" = NaN\n'
        "}"
    )
    assert output == ret["data"]


@pytest.mark.asyncio
async def test_to_json_fail_case(hub):
    result = await hub.exec.core.conversion.to_json(data="")
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for json conversion is empty" in result["comment"]

    result = await hub.exec.core.conversion.to_json(data=None)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for json conversion is empty" in result["comment"]
