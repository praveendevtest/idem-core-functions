import pytest


@pytest.mark.asyncio
async def test_distinct(hub):
    data = ["a", "b", "a", "c", "d", "b"]
    expected_data = ["a", "b", "c", "d"]
    result = await hub.exec.core.collection.distinct(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert expected_data == ret.get("data")

    # mixed data types
    data = ["a", 1, "a", "c", "1", "b", 1]
    expected_data = ["a", 1, "c", "1", "b"]
    result = await hub.exec.core.collection.distinct(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert expected_data == ret.get("data")


@pytest.mark.asyncio
async def test_distinct_fail_case(hub):
    result = await hub.exec.core.collection.distinct(data="")
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for distinct is empty" in result["comment"]

    result = await hub.exec.core.collection.distinct(data=None)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for distinct is empty" in result["comment"]


@pytest.mark.asyncio
async def test_flatten(hub):
    data = [["a", "b"], [], ["c"]]
    expected_data = ["a", "b", "c"]
    result = await hub.exec.core.collection.flatten(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert expected_data == ret.get("data")

    complicated = [[1, [2]], (3, 4, {5, 6}, 7), 8, "9"]
    expected_data = [1, 2, 3, 4, 5, 6, 7, 8, "9"]
    result = await hub.exec.core.collection.flatten(data=complicated)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert expected_data == ret.get("data")


@pytest.mark.asyncio
async def test_flatten_fail_case(hub):
    result = await hub.exec.core.collection.flatten(data="")
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for flatten is empty" in result["comment"]

    result = await hub.exec.core.collection.flatten(data=None)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for flatten is empty" in result["comment"]


@pytest.mark.asyncio
async def test_element(hub):
    data = ["a", "b", "c"]
    result = await hub.exec.core.collection.element(data=data, index=1)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert "b" == ret.get("data")

    result = await hub.exec.core.collection.element(data=data, index=3)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert "a" == ret.get("data")

    result = await hub.exec.core.collection.element(data=data, index=-1)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert "c" == ret.get("data")

    complicated = [[1, [2]], (3, 4, {5, 6}, 7), 8, "9"]
    expected_result = (3, 4, {5, 6}, 7)
    result = await hub.exec.core.collection.element(data=complicated, index=1)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert expected_result == ret.get("data")


@pytest.mark.asyncio
async def test_element_fail_case(hub):
    result = await hub.exec.core.collection.element(data="", index=1)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for element is empty" in result["comment"]

    result = await hub.exec.core.collection.element(data=None, index=1)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for element is empty" in result["comment"]


@pytest.mark.asyncio
async def test_length(hub):
    data = ["a", "b", "c"]
    result = await hub.exec.core.collection.length(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert 3 == ret.get("data")

    dict_data = {
        ("x", "y", "z"): "Elder Wand",
        2: "Expecto",
        3: "Patronum",
        4: "Avada",
        5: "Kedavra",
        6: float("nan"),
    }
    result = await hub.exec.core.collection.length(data=dict_data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert 6 == ret.get("data")

    data = "test data"
    result = await hub.exec.core.collection.length(data=data)
    assert result["result"], result["comment"]
    assert result.get("ret")
    ret = result.get("ret")
    assert 9 == ret.get("data")


@pytest.mark.asyncio
async def test_length_fail_case(hub):
    result = await hub.exec.core.collection.length(data="")
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for length is empty" in result["comment"]

    result = await hub.exec.core.collection.length(data=None)
    assert not result["result"], result["comment"]
    assert not result["ret"]
    assert "data for length is empty" in result["comment"]
