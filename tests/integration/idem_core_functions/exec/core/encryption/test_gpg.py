import gnupg
import pytest


@pytest.mark.asyncio
async def test_gpg_encrypt_empty_data(hub):
    result = await hub.exec.core.encryption.gpg.encrypt(data="", recipients="invalid")

    assert not result["result"]
    assert "data for gpg_encrypt is empty" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_encrypt_failure(hub):
    data = "This is a encryption test"
    result = await hub.exec.core.encryption.gpg.encrypt(data=data, recipients="invalid")

    assert not result["result"]
    assert "invalid recipient" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_encrypt_success(hub):
    data = "This is a encryption test"
    result = await hub.exec.core.encryption.gpg.encrypt(
        data=data, symmetric=True, passphrase="test"
    )

    assert result["result"]
    assert "encryption ok" in str(result["comment"])
    assert result["ret"]
    # The encrypted data is different everytime
    assert result["ret"]["data"]


@pytest.mark.asyncio
async def test_gpg_decrypt_empty_message(hub):
    result = await hub.exec.core.encryption.gpg.decrypt("")

    assert not result["result"]
    assert "message for gpg_decrypt is empty" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_decrypt_failure(hub):
    data = "This is for decryption test"
    # cannot use as fixture as it is cached and no easy way to clear the cache
    gpg = gnupg.GPG()
    response = gpg.encrypt(data=data, recipients=[], passphrase="test", symmetric=True)

    # not providing passphrase - so expecting it to fail
    result = await hub.exec.core.encryption.gpg.decrypt(
        str(response.data.decode("utf-8"))
    )

    assert not result["result"]
    assert "decryption failed" in str(result["comment"])
    assert not result["ret"]


@pytest.mark.asyncio
async def test_gpg_decrypt_success(hub):
    data = "This is for decryption test"
    # cannot use as fixture as it is cached and no easy way to clear the cache
    gpg = gnupg.GPG()
    response = gpg.encrypt(data=data, recipients=[], passphrase="test", symmetric=True)
    result = await hub.exec.core.encryption.gpg.decrypt(
        message=str(response.data.decode("utf-8")), passphrase="test"
    )

    assert result["result"]
    assert "decryption ok" in str(result["comment"])
    assert result["ret"]
    assert "This is for decryption test" == result["ret"]["data"]
